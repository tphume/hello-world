package subtractor

import "testing"

func TestSubtractInteger(t *testing.T) {
	if res := SubtractInteger(20, 10); res != 10 {
		t.Fatalf("expected [10], got = [%v]", res)
	}
}
