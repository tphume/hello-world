ARG GO_VERSION=1.13.6

FROM golang:${GO_VERSION}-alpine AS dev

# Set environment variables
ENV GO111MODULE="on" \
    CGO_ENABLED=0 \
    GOOS=linux

ENV APP_PATH="/hello-world" \
    APP_NAME="hello-world"

# Install git
RUN apk add --update git

# Change app directory
WORKDIR ${APP_PATH}

# Cache Go modules (No outside dependencies yet)
# COPY go.mod .
# COPY go.sum .

# Copy rest of the code
COPY . .

# Build binray files
RUN go build -ldflags="-s -w" -o  ${APP_NAME} ${APP_PATH}/cmd/hello-world/${APP_NAME}.go
RUN chmod +x ${APP_NAME}

# Final build image
FROM alpine AS prod

# Set environment variables
ENV APP_PATH="/hello-world" \
    APP_NAME="hello-world"

# Change app directory
WORKDIR ${APP_PATH}

# Copy binary from previous stage
COPY --from=dev ${APP_PATH}/${APP_NAME} ${APP_NAME}

ENTRYPOINT ["/hello-world/hello-world"]