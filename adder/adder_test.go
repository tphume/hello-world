package adder

import "testing"

func TestAddInteger(t *testing.T) {
	if res := AddInteger(10, 20); res != 30 {
		t.Fatalf("expected [10], got = [%v]", res)
	}
}
